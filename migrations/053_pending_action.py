""""Peewee migrations -- 053_pending_action.py

Replace the 'state' field of user_login with a pending_action field.

"""
import datetime as dt
from enum import IntEnum
import peewee as pw
import playhouse.postgres_ext as pw_ext

SQL = pw.SQL


def migrate(migrator, database, fake=False, **kwargs):
    migrator.add_fields("user_login", pending_action=pw_ext.JSONField(null=True))

    if not fake:
        migrator.run()
        migrator.sql(
            """
        UPDATE user_login
           SET pending_action = state
         WHERE state::text not in ('"Verified"', '"Deleted"', '"Banned"')
        """
        )

        migrator.sql(
            """
        UPDATE user_login
           SET pending_action = json_build_object('Verify',
                                                  json_extract_path(state, 'Unverified'))
         WHERE json_extract_path(state, 'Unverified') is not null
        """
        )

    migrator.remove_fields("user_login", "state")


def rollback(migrator, database, fake=False, **kwargs):
    migrator.add_fields("user_login", state=pw_ext.JSONField(null=True))
    if not fake:
        migrator.run()
        migrator.sql(
            """
        UPDATE user_login
           SET state = CASE u.status
                       WHEN 0 THEN to_json('Verified'::text)
                       WHEN 5 THEN to_json('Banned'::text)
                       WHEN 10 THEN to_json('Deleted'::text)
                       END
          FROM "user" u
         WHERE u.uid = user_login.uid
        """
        )
        migrator.sql(
            """
        UPDATE user_login
           SET state = pending_action
         WHERE json_extract_path(pending_action, 'NewEmail') is not null
            OR json_extract_path(pending_action, 'Reset') is not null
        """
        )
        migrator.sql(
            """
        UPDATE user_login
           SET state = json_build_object('Unverified',
                                         json_extract_path(pending_action, 'Verify'))
         WHERE json_extract_path(pending_action, 'Verify') is not null
        """
        )
    migrator.remove_fields("user_login", "pending_action")
    migrator.add_not_null("user_login", "state")
