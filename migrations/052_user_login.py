""""Peewee migrations -- 052_user_login.py

Add a table to hold login information for each user.

Make a backup before you run this migration, because the rollback
cannot completely recover the previous state.

Hashes for "password":

Flask: $2b$12$mqihsrprYYyRQ5AImyCO6u4frV8.QkHcXGwaqataeN22xsB6LuLFa
ovarit-auth: $pbkdf2-sha256$i=10000,l=32$/7RwfFHDuco+qjtYKPqt6A$Ejd55//46CNDwYiYI1bekWSdy6SZzSKFM3WZP12A908

"""
import datetime as dt
from enum import IntEnum
import peewee as pw
import playhouse.postgres_ext as pw_ext

SQL = pw.SQL

# based on the following Rust code.
# pub struct UserLogin {
#     pub uid: String,
#     pub email: String,
#     pub parsed_hash: String,
#     pub state: UserState,
# }
#
# pub enum UserState {
#     Unverified { delete: String, code: String },
#     NewEmail { email: String, code: String },
#     Verified,
#     Reset { code: String },
#     Banned,
#     Deleted,
# }


class UserCrypto(IntEnum):
    """Password hash algorithm."""

    BCRYPT = 1
    REMOTE = 2  # password stored on remote auth server


class UserStatus(IntEnum):
    """User's login capability status."""

    OK = 0
    PROBATION = 1  # New, with email not yet confirmed.
    BANNED = 5  # site-ban
    DELETED = 10


def migrate(migrator, database, fake=False, **kwargs):
    @migrator.create_model
    class UserLogin(pw.Model):
        uid = pw.ForeignKeyField(
            column_name="uid", model=migrator.orm["user"], field="uid"
        )
        email = pw.CharField(null=False)
        parsed_hash = pw.CharField(null=False)
        state = pw_ext.JSONField(null=False)

        def __repr__(self):
            return f"<UserLogin{self.uid}>"

        class Meta:
            table_name = "user_login"

    if not fake:
        migrator.run()
        migrator.sql(
            "GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE user_login TO ovarit_auth;"
        )

        User = migrator.orm["user"]
        UserMetadata = migrator.orm["user_metadata"]
        SiteMetadata = migrator.orm["site_metadata"]
        Sub = migrator.orm["sub"]
        SubSubscriber = migrator.orm["sub_subscriber"]
        Notification = migrator.orm["notification"]

        users = User.select(User.uid, User.email, User.status)

        # Create UserLogin entries for all the users, with blank
        # password hashes.

        # If this is a production database with Keycloak,
        # you will need to copy the password hashes from the keycloak
        # database, doing the right thing for users who have a
        # "remote_uid" entry in user_metadata.

        # If this is a development database without Keycloak, you will
        # need to update the password hashes for all your users to
        # something compatible with ovarit-auth, such as:
        # $pbkdf2-sha256$i=10000,l=32$/7RwfFHDuco+qjtYKPqt6A$Ejd55//46CNDwYiYI1bekWSdy6SZzSKFM3WZP12A908
        # AKA "password"

        # If this is a development database that was running without
        # the email requirement, any users who don't have emails will
        # be set to deleted status.

        user_logins = []
        status_map = {
            UserStatus.OK: "Verified",
            UserStatus.BANNED: "Banned",
            UserStatus.DELETED: "Deleted",
        }
        for u in users:
            login = {
                "uid": u.uid,
                "email": u.email or "",
                "parsed_hash": "",
            }
            if u.status in status_map.keys():
                # There are a few users who registered without supplying emails.
                # Mark then as deleted.
                if login["email"] == "" and u.status != UserStatus.DELETED:
                    User.update(status=UserStatus.DELETED).where(
                        User.uid == u.uid
                    ).execute()
                    u.status = UserStatus.DELETED

                login["state"] = status_map[u.status]
                user_logins.append(login)
            else:
                # The only other choice is UserStatus.PROBATION.
                # These are users that never logged in by clicking the email link.
                # Previously sent email confirmation links that were
                # never used are incompatible with ovarit_auth.
                # Delete those users entirely.
                UserMetadata.delete().where(UserMetadata.uid == u.uid).execute()
                sids = [
                    ss.sid
                    for ss in SubSubscriber.select(SubSubscriber.sid).where(
                        SubSubscriber.uid == u.uid
                    )
                ]
                Sub.update(subscribers=Sub.subscribers - 1).where(
                    Sub.sid << sids
                ).execute()
                SubSubscriber.delete().where(SubSubscriber.uid == u.uid).execute()
                # It's possible for mention notifications to be sent to PROBATION
                # users.
                Notification.delete().where(Notification.receivedby == u.uid).execute()
                User.delete().where(User.uid == u.uid).execute()

        UserLogin.insert_many(user_logins).execute()

        # Remove any attempted email changes that were never confirmed, since
        # the links sent are incompatible with ovarit-auth.
        UserMetadata.delete().where(UserMetadata.key == "pending_email").execute()

        # Remove the email_verified flag, which is no longer needed.
        UserMetadata.delete().where(UserMetadata.key == "email_verified").execute()

        # Remove the now obsolete option to have local password hashes for
        # pre-Keycloak users.
        UserMetadata.delete().where(UserMetadata.key == "auth_source").execute()

        # Remove obsolete registration configuration settings.
        SiteMetadata.delete().where(
            SiteMetadata.key
            << [
                "site.enable_security_question",
                "site.require_captchas",
                "secquestion",
            ]
        ).execute()

    migrator.remove_fields("user", "crypto", "email", "password")


def rollback(migrator, database, fake=False, **kwargs):
    migrator.add_fields(
        "user",
        crypto=pw.IntegerField(null=True),
        email=pw.CharField(null=True),
        password=pw.CharField(null=True),
    )
    migrator.remove_model("user_login")
    if not fake:
        SiteMetadata = migrator.orm["site_metadata"]
        SiteMetadata.insert_many(
            [
                {"key": "site.enable_security_question", "value": "0"},
                {"key": "site.require_captchas", "value": "0"},
            ]
        ).execute()
