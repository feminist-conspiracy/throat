"""Peewee migrations -- 046_slugify.py

Add the URL slug calculated from the post title to the sub_post table.
"""
import datetime as dt
import peewee as pw
from slugify import slugify

SQL = pw.SQL


def migrate(migrator, database, fake=False, **kwargs):
    SubPost = migrator.orm["sub_post"]
    migrator.add_fields("sub_post", slug=pw.CharField(default="_"))
    if not fake:
        migrator.run()
        for post in SubPost.select(SubPost.pid, SubPost.title):
            slug = slugify(post.title, max_length=80)
            slug = slug if slug else "_"
            SubPost.update(slug=slug).where(SubPost.pid == post.pid).execute()


def rollback(migrator, database, fake=False, **kwargs):
    migrator.remove_fields("sub_post", "slug")
