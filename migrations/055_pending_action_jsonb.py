""""Peewee migrations -- 055_pending_action_jsonb.py

Convert the pending_action field of user_login to JSONB.

"""
import datetime as dt
from enum import IntEnum
import peewee as pw

SQL = pw.SQL


def migrate(migrator, database, fake=False, **kwargs):
    if not fake:
        migrator.sql(
            """
        ALTER TABLE user_login
          ALTER COLUMN pending_action TYPE jsonb
          USING pending_action::text::jsonb
        """
        )
        migrator.sql(
            """
        CREATE INDEX userlogin_code_verify ON user_login ((pending_action->'Verify'->>'code'))
        """
        )
        migrator.sql(
            """
        CREATE INDEX userlogin_code_reset ON user_login ((pending_action->'Reset'->>'code'))
        """
        )


def rollback(migrator, database, fake=False, **kwargs):
    if not fake:
        migrator.sql(
            """
        ALTER TABLE user_login
          ALTER COLUMN pending_action TYPE json
          USING pending_action::text::json
        """
        )
        migrator.sql(
            """
        DROP INDEX userlogin_code_verify
        """
        )
        migrator.sql(
            """
        DROP INDEX userlogin_code_reset
        """
        )
