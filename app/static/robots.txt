User-Agent: *
Disallow: /*.rss
Disallow: /register
Disallow: /login
Disallow: /o/*/sublog
Disallow: /submit/*
Disallow: /oauth/*
Allow: /
