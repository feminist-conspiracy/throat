""" API endpoints. """

from flask import Blueprint, jsonify, request
from flask_babel import _
from .. import misc
from ..models import Sub, SubPost, SubRule

API = Blueprint("apiv3", __name__)


@API.errorhandler(429)
def ratelimit_handler(_):
    return jsonify(msg="Rate limited. Please try again later"), 429


@API.route("/sub", methods=["GET"])
def search_sub():
    """Search for subs matching the query parameter.  Return, for each sub
    found, its name and a list of the post types allowed."""
    query = request.args.get("query", "")
    if len(query) < 3 or not misc.allowedNames.match(query):
        return jsonify(results=[])

    query = "%" + query + "%"
    subs = Sub.select(Sub.name).where(Sub.name**query).limit(10).dicts()

    return jsonify(results=list(subs))


@API.route("/sub/rules", methods=["GET"])
def get_sub_rules():
    pid = request.args.get("pid", "")
    sub = (
        SubPost.select()
        .where(SubPost.pid == pid)
        .join(Sub)
        .where(Sub.sid == SubPost.sid)
        .dicts()
        .get()
    )
    rules = list(SubRule.select().where(SubRule.sid == sub["sid"]).dicts())
    for r in rules:
        r["text"] = r["text"][: (128 - len(_("Circle Rule: ")))]

    return jsonify(results=rules)
