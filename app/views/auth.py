""" Authentication endpoints and functions """
import uuid
import re
import requests
from peewee import fn
from flask import (
    Blueprint,
    request,
    redirect,
    abort,
    url_for,
    flash,
    jsonify,
)
from flask_login import current_user, login_user, login_required
from flask_babel import _
from .. import misc
from ..config import config
from ..misc import gevent_required
from ..misc import ratelimit, AUTH_LIMIT
from ..models import User, UserStatus, rconn

bp = Blueprint("auth", __name__)


def sanitize_serv(serv):
    serv = serv.replace("%253A", "%3A")
    return serv.replace("%252F", "%2F")


def generate_cas_token(uid):
    # Create Session Ticket and store it in Redis
    token = str(uuid.uuid4())
    rconn.setex(name="cas-" + token, value=uid, time=30)
    return token


def handle_cas_ok(uid):
    # 2 - Send the ticket over to `service` with the ticket parameter
    return redirect(
        sanitize_serv(request.args.get("service"))
        + "&ticket="
        + generate_cas_token(uid)
    )


@bp.route("/proxyValidate", methods=["GET"])
@ratelimit(AUTH_LIMIT)
def sso_proxy_validate():
    if not request.args.get("ticket") or not request.args.get("service"):
        abort(400)

    pipe = rconn.pipeline()
    pipe.get("cas-" + request.args.get("ticket"))
    pipe.delete("cas-" + request.args.get("ticket"))
    red_c = pipe.execute()

    if red_c:
        try:
            user = User.get((User.uid == red_c[0].decode()) & (User.status << (0, 100)))
        except User.DoesNotExist:
            return (
                "<cas:serviceResponse xmlns:cas='http://www.yale.edu/tp/cas'>"
                '<cas:authenticationFailure code="INVALID_TICKET">'
                + _("User not found or invalid ticket")
                + "</cas:authenticationFailure></cas:serviceResponse>",
                401,
            )

        return (
            "<cas:serviceResponse xmlns:cas='http://www.yale.edu/tp/cas'>"
            f"<cas:authenticationSuccess><cas:user>{user.name.lower()}</cas:user>"
            "</cas:authenticationSuccess></cas:serviceResponse>",
            200,
        )
    else:
        return (
            "<cas:serviceResponse xmlns:cas='http://www.yale.edu/tp/cas'>"
            '<cas:authenticationFailure code="INVALID_TICKET">'
            + _("User not found or invalid ticket")
            + "</cas:authenticationFailure></cas:serviceResponse>",
            401,
        )


@bp.route("/login/with-token/<token>")
def login_with_token(token):
    flash(_("The link you used is invalid or has expired."), "error")
    return redirect(url_for("home.index"))


@bp.route("/login")
def login():
    """This exists for the url-for call in flask-login, which wants
    something.  Configure your load balancer to send this route
    to ovarit-auth."""
    abort(404)


@bp.route("/login_test_only/<name>", methods=["POST"])
def test_login(name):
    """Test-only login route"""
    if not config.app.testing:
        abort(403)

    if current_user.is_authenticated:
        abort(400)

    try:
        user = User.get(fn.Lower(User.name) == name.lower())
    except User.DoesNotExist:
        abort(403)

    if user.status != UserStatus.OK:
        abort(403)

    theuser = misc.load_user(user.uid)
    login_user(theuser, remember=False)

    return jsonify(status="OK"), 200


@bp.route("/auth/matrix", methods=["POST"])
@gevent_required  # Makes request from Matrix server.
@login_required
def get_ticket():
    """Returns a CAS ticket for the current user"""
    token = generate_cas_token(current_user.uid)
    # Not using safe_requests since we're supposed to trust this server.
    uri = f"{config.matrix.homeserver}/_matrix/client/r0/login/cas/ticket?redirectUrl=throat%3A%2F%2F&ticket={token}"
    resp = requests.get(uri, allow_redirects=False)

    if resp.status_code == 200:
        matches = re.search(r"href=\".+\?loginToken=(.+)\" class=.+?>", resp.text)
        return jsonify(token=matches.groups()[0])

    return jsonify(error="absolutely"), 400
