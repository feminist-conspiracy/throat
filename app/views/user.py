""" Profile and settings endpoints """
from datetime import datetime, timedelta
from peewee import fn, JOIN
from flask import Blueprint, render_template, abort, redirect, url_for, flash, request
from flask_login import login_required, current_user
from flask_babel import _
from .. import misc
from ..config import config
from ..misc import engine
from ..misc import ratelimit, AUTH_LIMIT, limit_pagination
from ..forms import (
    CsrfTokenOnlyForm,
    EditIgnoreForm,
    CreateUserMessageForm,
)
from ..models import (
    User,
    UserStatus,
    UserUploads,
    UserMessageBlock,
    UserContentBlock,
    UserContentBlockMethod,
    UserNameHistory,
    UserMetadata,
    Sub,
    SubMod,
    SubPost,
    SubPostComment,
    UserSaved,
    InviteCode,
)
from ..badges import badges as badges_module

bp = Blueprint("user", __name__)


def view_deleted_user(user):
    return engine.get_template("user/profile.html").render(
        {
            "user": user,
            "level": None,
            "progress": None,
            "postCount": None,
            "commentCount": None,
            "givenScore": None,
            "invitecodeinfo": None,
            "badges": None,
            "owns": None,
            "mods": None,
            "habits": None,
            "target_user_is_admin": None,
            "target_user_is_blocked": None,
            "msgform": None,
            "ignform": None,
            "name_history": None,
            "block_dms": False,
        }
    )


@bp.route("/u/<user>")
def view(user):
    """WIP: View user's profile, posts, comments, badges, etc"""
    try:
        user = User.get(fn.lower(User.name) == user.lower())
    except User.DoesNotExist:
        if current_user.is_authenticated:
            on = UserNameHistory.uid == User.uid
            if not (current_user.is_admin() or current_user.is_a_mod):
                display_cutoff = datetime.utcnow() - timedelta(
                    days=config.site.username_change.display_days
                )
                on = on & (UserNameHistory.changed > display_cutoff)
            user = (
                User.select()
                .join(UserNameHistory, on=on)
                .where(fn.lower(UserNameHistory.name) == user.lower())
            )
            if not user:
                abort(404)
            user = user.first()
        else:
            abort(404)

    if user.status == 10 and not current_user.is_admin():
        return view_deleted_user(user)

    modsquery = (
        SubMod.select(Sub.name, SubMod.power_level)
        .join(Sub)
        .where((SubMod.uid == user.uid) & (~SubMod.invite))
    )
    owns = [x.sub.name for x in modsquery if x.power_level == 0]
    mods = [x.sub.name for x in modsquery if 1 <= x.power_level <= 2]
    invitecodeinfo = misc.getInviteCodeInfo(user.uid)
    badges = badges_module.badges_for_user(user.uid)
    pcount = SubPost.select().where(SubPost.uid == user.uid).count()
    ccount = SubPostComment.select().where(SubPostComment.uid == user.uid).count()
    user_is_admin = misc.is_target_user_admin(user.uid)
    habit = Sub.select(Sub.name, fn.Count(SubPost.pid).alias("count")).join(
        SubPost, JOIN.LEFT_OUTER, on=(SubPost.sid == Sub.sid)
    )
    habit = (
        habit.where(SubPost.uid == user.uid)
        .group_by(Sub.sid)
        .order_by(fn.Count(SubPost.pid).desc())
        .limit(10)
    )

    name_history = ""
    if current_user.is_authenticated:
        username_history = UserNameHistory.select(UserNameHistory.name).where(
            UserNameHistory.uid == user.uid
        )
        if not (current_user.is_admin() or current_user.is_a_mod):
            display_cutoff = datetime.utcnow() - timedelta(
                days=config.site.username_change.display_days
            )
            username_history = username_history.where(
                UserNameHistory.changed > display_cutoff
            )
        username_history = username_history.order_by(UserNameHistory.name).distinct()
        if username_history:
            if len(username_history) == 1:
                name_history = username_history.first().name
            else:
                names = [n.name for n in username_history]
                name_history = _(", ").join(names[:-1]) + _(" and ") + names[-1]

    level, xp = misc.get_user_level(user.uid)

    if xp > 0:
        currlv = (level**2) * 10
        nextlv = ((level + 1) ** 2) * 10

        required_xp = nextlv - currlv
        progress = ((xp - currlv) / required_xp) * 100
    else:
        progress = 0

    if user.upvotes_given is None:
        givenScore = misc.getUserGivenScore(user.uid)
    else:
        givenScore = (
            user.upvotes_given,
            user.downvotes_given,
            user.upvotes_given - user.downvotes_given,
        )

    block_dms = False
    messages = content = "show"
    if (
        current_user.uid != user.uid
        and not user_is_admin
        and not current_user.can_admin
    ):
        try:
            block_dms = (
                UserMetadata.get(
                    (UserMetadata.uid == user.uid) & (UserMetadata.key == "block_dms")
                ).value
                == "1"
            )
        except UserMetadata.DoesNotExist:
            block_dms = False

        try:
            UserMessageBlock.get(
                (UserMessageBlock.uid == user.uid)
                & (UserMessageBlock.target == current_user.uid)
            )
            block_dms = True
        except UserMessageBlock.DoesNotExist:
            pass

        blocked = (
            User.select(
                UserMessageBlock.id.is_null(False).alias("hide_messages"),
                UserContentBlock.method,
            )
            .join(
                UserMessageBlock,
                JOIN.LEFT_OUTER,
                on=(
                    (UserMessageBlock.uid == current_user.uid)
                    & (UserMessageBlock.target == user.uid)
                ),
            )
            .join(
                UserContentBlock,
                JOIN.LEFT_OUTER,
                on=(
                    (UserContentBlock.uid == current_user.uid)
                    & (UserContentBlock.target == user.uid)
                ),
            )
            .dicts()
            .get()
        )

        if blocked["hide_messages"]:
            messages = "hide"
        else:
            messages = "show"
        if blocked["method"] is None:
            content = "show"
        elif blocked["method"] == UserContentBlockMethod.BLUR:
            content = "blur"
        else:
            content = "hide"
    ignform = EditIgnoreForm(view_messages=messages, view_content=content)

    return engine.get_template("user/profile.html").render(
        {
            "user": user,
            "level": level,
            "progress": progress,
            "postCount": pcount,
            "commentCount": ccount,
            "givenScore": givenScore,
            "invitecodeinfo": invitecodeinfo,
            "badges": badges,
            "owns": owns,
            "mods": mods,
            "habits": habit,
            "target_user_is_admin": user_is_admin,
            "target_user_is_blocked": messages == "hide",
            "msgform": CreateUserMessageForm(),
            "ignform": ignform,
            "name_history": name_history,
            "block_dms": block_dms,
        }
    )


@bp.route("/u/<user>/posts", defaults={"page": None})
@bp.route("/u/<user>/posts/<int:page>")
@limit_pagination
def view_user_posts(user, page):
    """WIP: View user's recent posts"""
    try:
        user = User.get(fn.Lower(User.name) == user.lower())
    except User.DoesNotExist:
        abort(404)
    if user.status == 10 and not current_user.is_admin():
        return view_deleted_user(user)

    include_deleted_posts = user.uid == current_user.uid or current_user.is_admin()
    if not include_deleted_posts and current_user.is_a_mod:
        modded_subs = [s.sid for s in misc.getModSubs(current_user.uid, 2)]
        if modded_subs:
            include_deleted_posts = modded_subs

    page_info = misc.getPostList(
        misc.postListQueryBase(
            include_deleted_posts=include_deleted_posts,
            noAllFilter=not current_user.is_admin(),
            noUserFilter=True,
            sort="new",
        ).where(User.uid == user.uid),
        sort="new",
        page=page,
        first=request.args.get("first"),
        after=request.args.get("after"),
        last=request.args.get("last"),
        before=request.args.get("before"),
    )
    return render_template(
        "userposts.html",
        page_info=page_info,
        route="user.view_user_posts",
        user=user,
    )


@bp.route("/u/<user>/savedposts", defaults={"page": None})
@bp.route("/u/<user>/savedposts/<int:page>")
@login_required
@limit_pagination
def view_user_savedposts(user, page):
    """WIP: View user's saved posts"""
    if current_user.name.lower() == user.lower():
        page_info = misc.getPostList(
            misc.postListQueryBase(noAllFilter=True, noUserFilter=True)
            .join(UserSaved, on=(UserSaved.pid == SubPost.pid))
            .where(UserSaved.uid == current_user.uid),
            page=page,
            first=request.args.get("first"),
            after=request.args.get("after"),
            last=request.args.get("last"),
            before=request.args.get("before"),
        )
        return render_template(
            "userposts.html",
            page_info=page_info,
            route="user.view_user_savedposts",
            user=current_user,
        )
    else:
        abort(403)


@bp.route("/u/<user>/comments", defaults={"page": None})
@bp.route("/u/<user>/comments/<int:page>")
@limit_pagination
def view_user_comments(user, page):
    """WIP: View user's recent comments"""
    try:
        user = User.get(fn.Lower(User.name) == user.lower())
    except User.DoesNotExist:
        abort(404)
    if user.status == 10 and not current_user.is_admin():
        return view_deleted_user(user)

    include_deleted_comments = user.uid == current_user.uid or current_user.is_admin()
    if not include_deleted_comments and current_user.is_a_mod:
        modded_subs = [s.sid for s in misc.getModSubs(current_user.uid, 2)]
        if modded_subs:
            include_deleted_comments = modded_subs

    page_info = misc.getUserComments(
        user.uid,
        page=page,
        first=request.args.get("first"),
        after=request.args.get("after"),
        last=request.args.get("last"),
        before=request.args.get("before"),
        include_deleted_comments=include_deleted_comments,
    )
    postmeta = misc.get_postmeta_dicts((c["pid"] for c in page_info["comments"]))
    return render_template(
        "usercomments.html", user=user, page_info=page_info, postmeta=postmeta
    )


@bp.route("/uploads", defaults={"page": None})
@bp.route("/uploads/<int:page>")
@login_required
def view_user_uploads(page):
    """View user uploads"""
    uploads = (
        UserUploads.select(
            *(
                [
                    UserUploads.xid,
                    UserUploads.thumbnail,
                    UserUploads.pid,
                    UserUploads.fileid,
                ]
                + UserUploads.aliased_cursor_fields()
            )
        )
        .join(SubPost)
        .where((UserUploads.uid == current_user.uid) & (SubPost.deleted != 1))
    )

    page_info = misc.paginate_query(
        UserUploads,
        uploads,
        page=page,
        first=request.args.get("first"),
        after=request.args.get("after"),
        last=request.args.get("last"),
        before=request.args.get("before"),
        page_size=30,
    )
    return render_template("uploads.html", page_info=page_info)


@bp.route("/settings/invite")
@login_required
def invite_codes():
    if not config.site.require_invite_code:
        return redirect("/settings")

    codes = (
        InviteCode.select()
        .where(InviteCode.user == current_user.uid)
        .order_by(InviteCode.created.desc())
    )
    maxcodes = int(misc.getMaxCodes(current_user.uid))
    created = codes.count()
    avail = 0
    if (maxcodes - created) >= 0:
        avail = maxcodes - created
    return engine.get_template("user/settings/invitecode.html").render(
        {
            "codes": codes,
            "created": created,
            "max": maxcodes,
            "avail": avail,
            "user": User.get(User.uid == current_user.uid),
            "csrf_form": CsrfTokenOnlyForm(),
        }
    )


@bp.route("/settings/subs")
@login_required
def edit_subs():
    return engine.get_template("user/topbar.html").render({})


@bp.route("/settings")
@login_required
def edit_user():
    # No longer used except for building the URL.
    abort(404)


@bp.route("/settings/account/confirm-email/<token>")
@ratelimit(AUTH_LIMIT)
def confirm_email_change(token):
    flash(_("The link you used is invalid or has expired."), "error")
    return redirect(url_for("home.index"))


@bp.route("/settings/delete")
@login_required
def delete_account():
    # No longer used except for building the URL.
    abort(404)


@bp.route("/reset/<token>")
def password_reset(token):
    """The page that actually resets the password"""
    flash(_("Password reset link was invalid or expired"), "error")
    return redirect(url_for("home.index"))


@bp.route("/settings/blocks", defaults={"page": 1})
@bp.route("/settings/blocks/<int:page>")
@login_required
def view_ignores(page):
    """View user's blocked users."""
    if current_user.can_admin:
        abort(404)
    menu = request.args.get("menu", "user")

    def add_form(ig):
        messages = "hide" if ig["hide_messages"] else "show"
        if ig["method"] is None:
            content = "show"
        elif ig["method"] == UserContentBlockMethod.HIDE:
            content = "hide"
        else:
            content = "blur"
        ig["form"] = EditIgnoreForm(view_messages=messages, view_content=content)
        return ig

    query = (
        User.select(
            User.name,
            User.uid.alias("target"),
            UserMessageBlock.id.is_null(False).alias("hide_messages"),
            UserContentBlock.method,
        )
        .join(
            UserMessageBlock,
            JOIN.LEFT_OUTER,
            on=(
                (UserMessageBlock.uid == current_user.uid)
                & (UserMessageBlock.target == User.uid)
            ),
        )
        .join(
            UserContentBlock,
            JOIN.LEFT_OUTER,
            on=(
                (UserContentBlock.uid == current_user.uid)
                & (UserContentBlock.target == User.uid)
            ),
        )
        .where(
            (User.status == UserStatus.OK)
            & (UserContentBlock.id.is_null(False) | UserMessageBlock.id.is_null(False))
        )
        .order_by(fn.lower(User.name))
        .paginate(page, 25)
        .dicts()
    )
    igns = [add_form(ig) for ig in query]

    return engine.get_template("user/ignores.html").render(
        {
            "igns": igns,
            "user": User.get(User.uid == current_user.uid),
            "page": page,
            "menu": menu,
        }
    )
