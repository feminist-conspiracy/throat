""" User-related forms """
from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField
from wtforms import HiddenField, SelectField
from wtforms.validators import DataRequired, Length
from wtforms.validators import Optional, Regexp
from flask_babel import lazy_gettext as _l


class OptionalIfFieldIsEmpty(Optional):
    """A custom field validator."""

    def __init__(self, field_name, *args, **kwargs):
        self.field_name = field_name
        super(OptionalIfFieldIsEmpty, self).__init__(*args, **kwargs)

    def __call__(self, form, field):
        other_field = form._fields.get(self.field_name)
        if other_field is None:
            raise Exception('no field named "{0}" in form'.format(self.field_name))
        if other_field.data == "":
            super(OptionalIfFieldIsEmpty, self).__call__(form, field)


class EditIgnoreForm(FlaskForm):
    """Edit User blocks form."""

    view_messages = SelectField(
        "",
        choices=[
            ("hide", _l("Block direct messages")),
            ("show", _l("Allow direct messages")),
        ],
    )
    view_content = SelectField(
        "",
        choices=[
            ("hide", _l("Hide posts and comments")),
            ("blur", _l("Blur posts and collapse comments")),
            ("show", _l("Show posts and comments")),
        ],
    )


class CreateUserMessageForm(FlaskForm):
    """CreateUserMessage form."""

    to = StringField(_l("to"), [Length(min=2, max=32), Regexp(r"[a-zA-Z0-9_-]+")])
    subject = StringField(
        _l("subject"), validators=[DataRequired(), Length(min=1, max=400)]
    )

    content = TextAreaField(
        _l("message"), validators=[DataRequired(), Length(min=1, max=16384)]
    )


class CreateUserMessageReplyForm(FlaskForm):
    """Form for replies to direct messages."""

    mid = HiddenField()
    content = TextAreaField(
        _l("message"), validators=[DataRequired(), Length(min=1, max=16384)]
    )


class LogOutForm(FlaskForm):
    """Logout form. This form has no fields.
    We only use it for the CSRF stuff"""

    pass
