"""Peewee migrations -- 006_funding_goal.py.

"""

import datetime as dt
from enum import IntEnum
import peewee as pw

try:
    import playhouse.postgres_ext as pw_pext
except ImportError:
    pass

SQL = pw.SQL


class MessageMailbox(IntEnum):
    """Mailboxes for direct messages."""

    INBOX = 200
    SENT = 201
    SAVED = 202
    ARCHIVED = 203  # Modmail only.
    TRASH = 204
    DELETED = 205


# Only need enough fields to make it generate the right SQL.
class User(pw.Model):
    uid = pw.CharField(primary_key=True, max_length=40)

    class Meta:
        table_name = "user"


class Message(pw.Model):
    mid = pw.AutoField()

    class Meta:
        table_name = "message"


def migrate(migrator, database, fake=False, **kwargs):
    """Write your migrations here."""

    # This migrator.create_model decorator has no effect on the
    # database since SiteMetadata already exists, but it adds the
    # model to the migrator so it can be used with the migrator's
    # database connection.
    @migrator.create_model
    class SiteMetadata(pw.Model):
        key = pw.CharField(null=True)
        value = pw.CharField(null=True)
        xid = pw.AutoField()

        class Meta:
            table_name = "site_metadata"

    if not fake:
        try:
            SiteMetadata.get(SiteMetadata.key == "site.funding_goal")
        except SiteMetadata.DoesNotExist:
            SiteMetadata.create(key="site.funding_goal", value="0")


def rollback(migrator, database, fake=False, **kwargs):
    """Write your rollback migrations here."""
    if not fake:
        SiteMetadata = migrator.orm["site_metadata"]
        SiteMetadata.delete().where(SiteMetadata.key == "site.funding_goal").execute()
