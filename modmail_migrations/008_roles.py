""""Peewee migrations -- 008_roles.py

Grant permissions to database users.
"""

table_permissions = {
    "customer_update_log": ["ovarit_auth", "throat_be", "throat_py"],
    "daily_uniques": ["throat_be"],
    "payments": ["ovarit_subs"],
    "visit": ["throat_be", "throat_py"],
}


def migrate(migrator, database, fake=False, **kwargs):
    if not fake:
        for table, users in table_permissions.items():
            for user in users:
                migrator.sql(
                    f'GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE "{table}" TO "{user}"'
                )


def rollback(migrator, database, fake=False, **kwargs):
    if not fake:
        for table, users in table_permissions.items():
            for user in users:
                migrator.sql(
                    f'REVOKE SELECT, INSERT, UPDATE, DELETE ON TABLE "{table}" FROM "{user}"'
                )
