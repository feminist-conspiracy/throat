# Compile the translations.
# This is done in its own step as the translations are used by both
# webpack and Flask.
FROM python:3.10-slim-bookworm AS translations
RUN pip install Babel
COPY app/translations /translations
RUN pybabel compile --directory=translations


FROM node:20-bookworm-slim as webpack
WORKDIR /node/
# Install our npm requirements
COPY package.json package-lock.json ./
RUN npm ci
# Build our static assets with webpack.
COPY webpack.config.js .
COPY --from=translations /translations ./app/translations
COPY app/static ./app/static
RUN npm run build


FROM python:3.10-slim-bookworm

# Install system packages.
RUN useradd -ms /bin/bash app

COPY scripts/deps.sh ./deps.sh
RUN /bin/bash deps.sh 2>&1

WORKDIR /python/
# Install our python requirements
USER app
ENV PATH="/home/app/.local/bin:${PATH}"
COPY requirements.txt ./requirements.txt
RUN pip3 install -r requirements.txt

# Create the app user and the application directory.
COPY --chown=app:app . /throat
WORKDIR /throat

# Pull in the compiled translations and static files.
COPY --from=translations --chown=app:app /translations /throat/app/translations
COPY --from=webpack --chown=app:app /node/app/static/gen /throat/app/static/gen
RUN DATABASE_NAME=throat DATABASE_ENGINE=PostgresqlDatabase flask digest compile

EXPOSE 5000

CMD ["./start_all.sh"]
