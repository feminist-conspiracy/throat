var path = require('path');

var webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const NodePolyfillPlugin = require("node-polyfill-webpack-plugin");
process.traceDeprecation = true;

module.exports = {
  entry: {
    main: ['./app/static/js/main.js'],
  },
  output: {
    path: path.resolve(__dirname, 'app/static/gen'),
    chunkFilename: '[id].js',
    publicPath: '/static/gen/'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components|ext)/,
        use: {
          loader: 'babel-loader',
        }
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
      {
        test: [/\.pot?$/, /\.mo$/],
        loader: require.resolve('messageformat-po-loader'),
        options: {
          biDiSupport: false,
          defaultCharset: null,
          defaultLocale: 'en',
          forceContext: false,
          pluralFunction: null,
          verbose: false
        }
      },
      {
        test: /\.svg$/,
        exclude: [/sprite\.svg/],
        loader: 'svg-inline-loader'
      }
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
        filename: '[name].css',
        chunkFilename: '[id].css'
    }),
    new NodePolyfillPlugin()
  ],
  node: false,
  devtool: 'source-map',
  devServer: {
    compress: true,
    hot: false,
    liveReload: false,
    port: 9000
  }
};
