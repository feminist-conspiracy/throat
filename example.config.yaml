# Any value in this file can be overridden by an environment variable
# named using the keys, in upper case, joined by underscores.  So for
# example, if the environment variable $SITE_SERVER_NAME is set, its
# value will be used instead of the server name declared below.

site:
  # Domain name at which the app is being served.  This must be
  # configured for websocket support (chat and push new posts) on
  # Safari and Apple WebKit.  For development, you can omit this.
  #server_name: "throat.example.com"

  # Prefix for subs (by default it's 's').
  # It can be anything EXCEPT 'u', 'c' or 'p'
  sub_prefix: 'o'

  # Maximum size of an uploaded file, in bytes.
  upload_max_size: 16777216

  # List of links that will be shown in the footer.
  # Privacy and ToS links cannot be removed, and the license link can't be modified
  footer:
    links:
      ToS: '/wiki/tos'
      Privacy: '/wiki/privacy'
      Canary: '/wiki/canary'
      Donate: '/wiki/donate'
      Bugs: 'https://github.com/Phuks-co/throat/issues'

  # Number of trusted proxies which set the X-Forwarded-For header
  # ahead of the application.  If you run the application behind
  # a load balancer, this should be set to 1 or more.
  trusted_proxy_count: 0

  # Use a function defined in the database for hot-sorting posts.
  # This requires additional database configuration; see README.md for
  # more information.
  custom_hot_sort: False

  # URL of the icon to be used for push notifications
  icon_url: 'https://phuks.co/static/img/icon.png'

  # Relative path to the logo. This MUST be a SVG file that will be embedded into the HTML of every page.
  logo: 'app/static/img/throat-logo.svg'

  # Enforces 2FA for administrators. This will remove _most_ privileges from administrators until they introduce
  # a timed one-time password.
  enable_totp: False

app:
  # host to pass to SocketIO when we start the application
  host: "localhost"

  # URL to a working redis server.
  # Used for websockets (if enabled)
  redis_url: 'redis://127.0.0.1:6379'

  # Whether to force all traffic to HTTPS.  If you terminate SSL with
  # gunicorn in production you should set this to True.  If you use
  # another server such as nginx or a load balancer to terminate SSL,
  # this should be False.  Ignored in debug mode.
  force_https: False

  # Secret key used to encrypt session cookies.
  # You can generate one by using `os.urandom(24)`
  # ///// YOU MUST CHANGE THIS VALUE \\\\\
  secret_key: 'PUT SOMETHING HERE'

  # Enables debug mode. Always set to False in a production environment
  debug: True

  # This sets a default logging config and disables the sub creation level check.
  # Always set to False in a production environment
  development: True

  # Available languages for users to select. This will also be used when trying to guess
  # the user's language.
  languages:
    - en
    - es
    - ru
    - sk

  # Fallback language when there is no accept-language header sent by the browser
  fallback_language: 'en'

cache:
  # Caching strategy to use.
  # Recommended values:
  # - 'null' (no caching)
  # - 'redis' (recommended)
  # - 'simple' (only for testing)
  type: 'redis'

  # Redis to use for caching (if enabled)
  redis_url: 'redis://127.0.0.1:6379'

storage:
  # One of: LOCAL, S3, S3_US_WEST, S3_US_WEST_OREGON, S3_EU_WEST,
  # S3_AP_SOUTHEAST, S3_AP_NORTHEAST, GOOGLE_STORAGE, AZURE_BLOBS,
  # CLOUDFILES
  provider: 'LOCAL'

  # For cloud providers, uncomment and set these:
  #key: '...'
  #secret: '...'
  #container: '...'

  # Access control for cloud providers.  Options are "public-read",
  # "private", or blank for no access control.
  # acl: private

  # If you use a S3-like service, you may use the S3 provider and set
  # the endpoint URL here.
  #endpoint_url: '...'

  # If the provider is local, and you want Throat to serve the files
  # instead of setting up nginx or another web server to do so,
  # uncomment these.  The server_url is the relative endpoint for the
  # files.  The thumbnails and uploads paths must be the same for the
  # local server to serve both.  Setting site.server_name above to the
  # address bound to the Throat server (typically localhost:5000 in
  # development) is also necessary to get the local server to work.
  #
  # Serving files from Throat is not recommended for production, but
  # may be useful for development.
  server: True
  server_url: '/files'

  thumbnails:
    # If you use a cloud provider, you may set a filename prefix here
    #filename_prefix: '...'

    # If provider is LOCAL, path where thumbnails will be stored (app
    # MUST have write access)
    path: './stor'
    # URL or relative path where thumbnails are served.  Ignored if
    # local server is enabled above.
    url: 'https://thumbs.example.com/'

  uploads:
    # Same rules as thumbnails
    path: './stor'
    url: 'https://usercontent.example.com/'

database:
  # Database engine. Possible values:
  # - PostgresqlDatabase
  # - playhouse.postgres_ext.PostgresqlExtDatabase
  # - playhouse.pool.PooledPostgresqlDatabase
  # - playhouse.pool.PooledPostgresqlExtDatabase
  engine: 'PostgresqlDatabase'

  host: 'localhost'
  port: 5432
  user: 'polsaker'
  password: 'throat'
  # Database name
  name: 'phuks'

ratelimit:
  # Rate limiting configuration is not required, but all configuration
  # variables for flask-limiter may be set here (in lowercase and
  # without the RATELIMIT_ prefix).  See
  # https://flask-limiter.readthedocs.io/en/stable/#configuration.

  # For development, you may want to change this to False.
  enabled: True

logging:
  # Configuration for logging.  This section is optional.
  # See the Python documentation for logging.config.
  version: 1
  handlers:
    console:
      class: logging.StreamHandler
      formatter: basic
  formatters:
    basic:
      format: '%(levelname)s:%(name)s:%(request.remote_addr)s:%(request.method)s %(request.path)s:%(message)s'
  loggers:
    engineio.server:
      level: WARNING
    socketio.server:
      level: WARNING
    peewee:
      level: WARNING
    app:
      level: DEBUG
    app.socketio:
      level: WARNING
    app.sql_timing:      # Logs SQL like the peewee logger, but with execution timing.
      level: DEBUG
    flask-limiter:
      level: WARNING
      handlers:
        - console
    geventwebsocket.handler:
      level: WARNING
    botocore:
      level: WARNING
  root:
    level: DEBUG
    handlers:
      - console

# notifications:
  # Firebase Cloud Messaging API key
  # fcm_api_key: ''

# Optional: Replace the onsite chatbox with a Matrix client
# The client is currently a work in progress and relies heavily on the server autojoining users to the desired channel
# on registration AND on the CAS integration with Throat
matrix:
  # If True the chatbox becomes a Matrix client, if False it will use the regular chat over websockets
  enabled: False
  # Matrix homeserver we'll connect to
  homeserver: 'https://phuks.co'
  # Room ID of the room we'll show in the chatbox
  default_room: '!RBQNbVLNepzbeBbkpz:phuks.co'
  # URL we will redirect users to when they try to use the bigger chat (/chat)
  webchat_url: 'https://chat.phuks.co'
