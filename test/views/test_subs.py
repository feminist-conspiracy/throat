import datetime
import json
import re
import pytest
import mock
import requests.exceptions
from flask import url_for
from test.utilities import register_user, csrf_token, create_sub, log_out_current_user
from app.models import Sub, PostTypeConfig


def get_error(data):
    reply = json.loads(data.decode("utf-8"))
    assert reply["status"] == "error"
    return reply["message"]


def get_error_html(data):
    f = re.findall(rb"<div class=\"error\".*?>(.+?)</div>", data)
    return f[0] if f else b""


def test_create_sub_error(client, user_info):
    register_user(client, user_info)
    rv = client.get(url_for("subs.create_sub"))
    assert rv.status_code == 200

    data = {"csrf_token": csrf_token(rv.data), "subname": "ñññ", "title": "Testing"}
    rv = client.post(url_for("subs.create_sub"), data=data, follow_redirects=True)
    assert b"Circle name has invalid characters" in get_error_html(rv.data)
    data["subname"] = "home"
    rv = client.post(url_for("subs.create_sub"), data=data, follow_redirects=True)
    assert b"Invalid circle name" in get_error_html(rv.data)
    data["subname"] = "test"
    rv = client.post(url_for("subs.create_sub"), data=data, follow_redirects=True)
    assert b"You must be at least level 2." in get_error_html(rv.data)


@pytest.mark.parametrize("test_config", [{"site": {"sub_creation_min_level": 0}}])
def test_create_sub(client, user_info, test_config):
    register_user(client, user_info)
    rv = client.get(url_for("subs.create_sub"))
    assert rv.status_code == 200

    data = {"csrf_token": csrf_token(rv.data), "subname": "test", "title": "Testing"}

    rv = client.post(url_for("subs.create_sub"), data=data, follow_redirects=True)
    assert not get_error_html(rv.data)
    assert b"/s/test" in rv.data

    # if we try again it should fail
    rv = client.post(url_for("subs.create_sub"), data=data, follow_redirects=True)
    assert b"Circle is already registered" == get_error_html(rv.data)


@pytest.mark.parametrize("test_config", [{"site": {"sub_creation_min_level": 0}}])
def test_submit_text_post(client, user_info, test_config):
    register_user(client, user_info)
    create_sub(client)
    rv = client.get(url_for("home.index", ptype="text", sub="test"))
    data = {"csrf_token": csrf_token(rv.data), "title": "f\u000A\u000A\u000A"}

    rv = client.post(
        url_for("subs.submit", ptype="text", sub="does_not_exist"),
        data=data,
        follow_redirects=True,
    )
    assert "Circle does not exist." == get_error(rv.data)
    rv = client.post(
        url_for("subs.submit", ptype="text", sub="test"),
        data=data,
        follow_redirects=True,
    )
    assert "Error in the 'Post type' field - This field is required." == get_error(
        rv.data
    )
    data["ptype"] = "text"
    rv = client.post(
        url_for("subs.submit", ptype="text", sub="test"),
        data=data,
        follow_redirects=True,
    )
    assert "Title is too short and/or contains whitespace characters." in get_error(
        rv.data
    )
    data["title"] = "Testing!"
    rv = client.post(
        url_for("subs.submit", ptype="text", sub="test"),
        data=data,
        follow_redirects=True,
    )
    assert rv.status == "200 OK"
    reply = json.loads(rv.data.decode("utf-8"))
    assert reply["status"] == "ok"


@pytest.mark.parametrize("test_config", [{"site": {"sub_creation_min_level": 0}}])
def test_submit_link_post(client, user_info, test_config):
    with mock.patch("requests.get", side_effect=requests.exceptions.HTTPError):
        register_user(client, user_info)
        create_sub(client)
        rv = client.get(url_for("home.index", ptype="text", sub="test"))
        data = {
            "csrf_token": csrf_token(rv.data),
            "title": "Testing link!",
            "ptype": "link",
        }

        rv = client.post(
            url_for("subs.submit", ptype="link", sub="test"),
            data=data,
            follow_redirects=False,
        )
        assert get_error(rv.data) == "No link provided."
        data["link"] = "https://google.com"
        rv = client.post(
            url_for("subs.submit", ptype="link", sub="test"),
            data=data,
            follow_redirects=False,
        )
        assert rv.status_code == 200
        reply = json.loads(rv.data.decode("utf-8"))
        assert reply["status"] == "ok"

        # Test anti-repost
        rv = client.post(
            url_for("subs.submit", ptype="link", sub="test"),
            data=data,
            follow_redirects=False,
        )
        assert get_error(rv.data) == "This link was recently posted on this sub."


@pytest.mark.parametrize("test_config", [{"site": {"sub_creation_min_level": 0}}])
def test_submit_poll_post(client, user_info, user2_info, test_config):
    register_user(client, user_info)
    create_sub(client)
    log_out_current_user(client)

    register_user(client, user2_info)
    rv = client.get(url_for("home.index", ptype="text", sub="test"))
    data = {
        "csrf_token": csrf_token(rv.data),
        "title": "Testing poll!",
        "ptype": "poll",
        "hideresults": "1",
    }
    rv = client.post(
        url_for("subs.submit", ptype="poll", sub="test"),
        data=data,
        follow_redirects=False,
    )
    assert get_error(rv.data) == "This post type is not allowed in this circle."
    sub = Sub.get(Sub.name == "test")
    PostTypeConfig.update(mods_only=False).where(
        (PostTypeConfig.sid == sub.sid) & (PostTypeConfig.ptype == 3)
    ).execute()
    rv = client.post(
        url_for("subs.submit", ptype="text", sub="test"),
        data=data,
        follow_redirects=False,
    )
    assert get_error(rv.data) == "Not enough poll options provided."
    data["options-0"] = "Test 1"
    data["options-1"] = "Test 2"
    data["options-2"] = "Test 3" * 60
    data["closetime"] = "asdf"
    rv = client.post(
        url_for("subs.submit", ptype="text", sub="test"),
        data=data,
        follow_redirects=False,
    )
    assert get_error(rv.data) == "Poll option text is too long."
    data["options-2"] = "Test 3"
    rv = client.post(
        url_for("subs.submit", ptype="text", sub="test"),
        data=data,
        follow_redirects=False,
    )
    assert get_error(rv.data) == "Invalid closing time."
    data["closetime"] = (
        datetime.datetime.utcnow() - datetime.timedelta(days=2)
    ).isoformat() + "Z"
    rv = client.post(
        url_for("subs.submit", ptype="text", sub="test"),
        data=data,
        follow_redirects=False,
    )
    assert get_error(rv.data) == "The closing time is in the past!"
    data["closetime"] = (
        datetime.datetime.utcnow() + datetime.timedelta(days=62)
    ).isoformat() + "Z"
    rv = client.post(
        url_for("subs.submit", ptype="text", sub="test"),
        data=data,
        follow_redirects=False,
    )
    assert get_error(rv.data) == "Poll closing time is too far in the future."
    data["closetime"] = (
        datetime.datetime.utcnow() + datetime.timedelta(days=2)
    ).isoformat() + "Z"
    rv = client.post(
        url_for("subs.submit", ptype="text", sub="test"),
        data=data,
        follow_redirects=False,
    )
    assert rv.status == "200 OK"
    reply = json.loads(rv.data.decode("utf-8"))
    assert reply["status"] == "ok"
    assert reply["pid"] == 1


@pytest.mark.parametrize("test_config", [{"site": {"sub_creation_min_level": 0}}])
def test_submit_invalid_post_type(client, user_info, test_config):
    register_user(client, user_info)
    create_sub(client)
    rv = client.get(url_for("home.index", ptype="text", sub="test"))
    data = {
        "csrf_token": csrf_token(rv.data),
        "title": "Testing link!",
    }

    rv = client.post(
        url_for("subs.submit", ptype="toast", sub="test"),
        data=data,
        follow_redirects=False,
    )
    assert rv.status_code == 404


@pytest.mark.parametrize("test_config", [{"site": {"sub_creation_min_level": 0}}])
def test_random_sub(client, user_info, test_config):
    register_user(client, user_info)
    create_sub(client)
    rv = client.get(url_for("subs.random_sub"), follow_redirects=False)
    assert rv.status_code == 302
    assert "/s/test" == rv.location
